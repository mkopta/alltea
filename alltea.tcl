#!/usr/bin/env wish
# AllTea
# Desktop application to track your tee
# Created by Martin Kopta <martin@kopta.eu>, 2008

wm title . "AllTea 1.0.0"

set counterFontSize  30 
set in               120
set count            $in
set counterEntryVar  $in
set counting         0

button .startButton  -text  "Start"    -command {startCounting}
button .stopButton   -text  "Stop"     -command {stopCounting} -state disable 
button .exitButton   -text  "Quit"     -command {exit}
label  .counterLabel -font  "Sans $counterFontSize" -textvariable count
entry  .counterEntry -textvariable counterEntryVar

grid .startButton  -column 1 -row 3
grid .stopButton   -column 2 -row 3
grid .exitButton   -column 3 -row 3
grid .counterEntry -column 1 -row 2 -columnspan 3
grid .counterLabel -column 1 -row 1 -columnspan 3 -pady 10

proc startCounting {} {
	global in
	global count
	global counting
	global counterEntryVar
	set counting 1
	set count $counterEntryVar
	.stopButton  configure -state normal
	.startButton configure -state disable
	countdown
}

proc stopCounting {} {
	global in
	global counting
	global count
	global counterEntryVar
	set counting 0
	set count $counterEntryVar
	.stopButton configure -state disable
	.startButton configure -state normal
}

proc countdown {} {
	global count
	global counting
	if {$counting} {
		incr count -1
		if {$count < 0} {
			set count "TEA"
				alert
				stopCounting
				return
		}
		after 1000 countdown
		return
	} else {
		return
	}
}

proc alert {} {
	for {set i 10} {$i < 100} {incr i} {
		if {[expr $i%2] == 0} then {
			.counterLabel config -foreground red
		} else {
			.counterLabel config -foreground yellow
		}
		after 50 update
	}
	.counterLabel config -foreground black
}
